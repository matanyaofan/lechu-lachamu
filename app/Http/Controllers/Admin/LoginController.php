<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/login';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
//        $this->middleware('guest')->except('logout');
	}

	public function get()
	{
		$user = Auth::user();
		return response()->json(["success" => !!$user, "user" => $user]);

	}

	public function login(Request $request)
	{

		if (Auth::attempt(['email' => $request->input("email"), 'password' => $request->password, "role" => 1], $request->remember)) {
			// Authentication passed...
			return response()->json(["success" => true, "data" => Auth::user()]);
		}
		return response()->json(["success" => false]);
	}


	public function logout(Request $request)
	{
		$this->guard()->logout();

		$request->session()->flush();

		$request->session()->regenerate();

		return response()->json(["success" => true]);
	}

}
