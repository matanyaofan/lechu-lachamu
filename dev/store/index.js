import Vue from "vue";
import Vuex from "vuex";
import AuthModule from "./modules/Auth.module";
import UserModule from "./modules/User.module";

Vue.use(Vuex);
const store = new Vuex.Store({
	state: {},
	modules: {
		AuthModule,
		UserModule
	}
});
window.ss = store;
export default store;
