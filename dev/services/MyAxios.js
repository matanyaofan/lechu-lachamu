/**
 * Created by elad on 22 מרץ 2017.
 */
import * as axios from "axios";
let $router = null;
export const setRouter = (router) => {
	$router = router;
};
const myAxios = axios.create({
	baseURL: "api/"
});
myAxios.interceptors.response.use(function (response) {
	// Do something with response data
	// return null;
	return response;
}, function (error) {
	// Do something with response error
	window.qq = error;
	const response = error.response;
	if (response.status == 401) {
		//TODO go to login page
		if ($router) {
			$router.replace("login");
		}
	} else {
		//TODO ALERT ERROR;
	}
	console.log(error);
	return Promise.reject(error);
});

export default myAxios;