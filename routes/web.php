<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\AdminRole;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'api'], function () {
	Route::post("login", "Admin\LoginController@login");
	Route::get("logout", "Admin\LoginController@logout");
	Route::group(['middleware' => AdminRole::class], function () {
		Route::get("auth", "Admin\LoginController@get");
		Route::resource("user", "Admin\UserController");
	});
});

Route::group(['prefix' => 'app'], function () {

});


Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('{page}', function () {
	return view('index');
})->where(['page' => '^((?!api).)*$']);
Route::get('{page}/{subs}', function () {
	return view('index');
})->where(['page' => '^((?!api).)*$', 'subs' => '.*']);
Route::get('{page}/{subs}/{sub2}', function () {
	return view('index');
})->where(['page' => '^((?!api).)*$', 'subs' => '.*', 'sub2' => '.*']);