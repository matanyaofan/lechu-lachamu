require('bootstrap');

import router from "./router";
import {sync} from "vuex-router-sync";
import Vue from "vue";
import VueRouter from "vue-router";
import store from "./store";
import {setRouter} from "./services/MyAxios";

Vue.use(VueRouter);
sync(store, router);

setRouter(router);
Vue.component("loader", require("./components/loader.vue"));
Vue.component("btn-loader", require("./components/BtnLoader.vue"));
Vue.component("modal-vue", require("./components/ModalVue.vue"));

const app = new Vue({
	store,
	router,
	render: function (ce) {
		return ce(require("./App.vue"));
	},
	components: {},
}).$mount('#app');