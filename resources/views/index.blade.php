<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>מערכת ניהול</title>
	<base href="/">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap-rtl.css">
	<link rel="stylesheet" href="css/style.css?date={{\File::lastModified("css/style.css")}}">
</head>
<body>
<div id="app"></div>
<script src="js/app.js?date={{\File::lastModified("js/app.js")}}"></script>
</body>
</html>
