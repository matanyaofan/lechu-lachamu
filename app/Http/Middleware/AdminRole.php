<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use function abort;

class AdminRole
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$user = Auth::user();
		if ($user && $user->role >= 1) {
			return $next($request);
		}
		abort(401, "ללא הרשאות התחברות");
	}
}
