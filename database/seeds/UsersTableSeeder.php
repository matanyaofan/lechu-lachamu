<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->insert([
			'name' => "admin",
			'email' => 'elad@closeapp.co.il',
//		    'tel' => '0547278666',
			'role' => '1',
			'password' => bcrypt('123456')
		]);
		DB::table('users')->insert([
			'name' => "customer",
			'email' => 'elad@gmail.com',
//		    'tel' => '0547278666',
//		    'role' => '1',
			'password' => bcrypt('123456')
		]);
	}
}
