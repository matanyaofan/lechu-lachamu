/**
 * Created by elad on 15 נובמבר 2016.
 */
import VueRouter from "vue-router";
import store from "./store";
import {types as authType} from "./store/modules/Auth.module";
import LoginView from "./view/auth/LoginView.vue";
import UsersView from "./view/user/UsersView.vue";


const routes = [
	{path: '/', component: UsersView, name: "home", meta: {'RequireAuth': true}},
	{path: '/users', component: UsersView, name: "home", meta: {'RequireAuth': true}},
	{path: '/login', component: LoginView, meta: {'freeAccess': true}}
];
const router = new VueRouter({
	history: false,
	mode: 'history',
	routes
});


router.beforeEach((to, from, next) => {
	if (to.matched.some(record => !record.meta.freeAccess)) {
		if (!store.getters.isConnected) {
			store.dispatch(authType.GET).then((response) => {
					if (!store.getters.isConnected) {
						next({
							path: '/login',
						});
					}
					else {
						next()
					}
				},
				() => {
					next({
						path: '/login',
					});
				}
			);
		} else {
			next();
		}
	} else {
		next()
	}
});



export default router
