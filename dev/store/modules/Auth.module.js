/**
 * Created by elad on 20 נובמבר 2016.
 */
import UserService from "../../services/AuthService";

export const types = {
	LOGIN: "auth/login",
	GET: "auth/get",
	PASSWORD: "auth/password",
	LOGOUT: "auth/logout"

}
const service = new UserService;
export default {

	state: {
		user: {}

	},
	getters: {
		isConnected(state){
			return !!state.user.id;
		},
		isManager(state){
			return state.user.role == 1;
		}
	},
	mutations: {
		[types.LOGIN](state, userLogin) {
			state.user = userLogin;
		},
		[types.LOGOUT](state){
			state.user = {};
		}

	},


	actions: {
		[types.LOGIN](state, form) {
			return service.login(form).then((response) => {
				if (response.success) {
					state.commit(types.LOGIN, response.data)
				}
				return response;
			});
		},
		[types.GET](state) {
			return service.getAuth().then((response) => {
				if (response.success) {
					state.commit(types.LOGIN, response.user)
				}
				return response.data;
			});
		},
		[types.PASSWORD](state, form) {
			return service.sendPassword(form).then((response) => {
				return response.data;
			});
		},
		[types.LOGOUT](context){
			return service.logout()
				.then(
					response => {
						if (response.success) {
							context.commit(types.LOGOUT);
						}
						return response;
					}
				);
		},

	},
	types: types
}