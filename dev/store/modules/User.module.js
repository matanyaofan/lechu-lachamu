/**
 * Created by elad on 20 נובמבר 2016.
 */
import UserService from "../../services/UserService";

export const types = {
    FETCH: "user/fetch",
    GET: "user/get",
    REMOVE: "user/remove",
    SAVE: "user/save",
    CREATE: "user/create",
    STORE: "user/store",
    UPDATE: "user/update",

};

const service = new UserService;


const USER = {
    id: null,
    user: null,
    last_name: null,
    tel: null,
    email: null,
    password: null,
    role: null


};


export default {

    state: {
        users: [],
        nowUser: {}
    },



    mutations: {


        [types.FETCH](state, users) {
            state.users = users;
        },

        [types.GET](state, user) {
            state.nowUser = user;
        },
        [types.CREATE](state, user) {
            state.nowUser = Object.assign({}, USER);
        },

        [types.REMOVE](state, user) {
            state.users = UserService.removeFromArray(state.users, user);
        },
        [types.SAVE](state, user) {
            state.users.push(user);
        },
        [types.STORE](state, user) {

            state.users.push(user);
        },
        [types.UPDATE](state, user) {
            state.users = UserService.UpdateArrayById(state.users, user);
        }

    },
    getters: {

    },
    actions: {
        // [types.FETCH](context, filter = {}) {
        //     return service.fetch(filter).then((response) => {
        //         context.commit(types.FETCH, response);
        //         return response
        //     });
        // },


        [types.FETCH](context, filters = {}) {
            const filterKey = JSON.stringify(filters);
            let response = service.fetch(filters).then((response) => {
                context.commit(types.FETCH, response);
                // cache.set(filterKey, response);
                return response;
            });
            // if (cache.has(filterKey)) {
            //     response = cache.get(filterKey);
            //     context.commit(types.FETCH, response);
            // }
            // return response;

        },


        [types.GET](context) {
            return service.get().then((response) => {
                context.commit(types.GET, response.user)
                return response
            })
        },









        [types.SAVE](context, user) {
            if (user.id) {
                return context.dispatch(types.UPDATE, user);
            } else {
                return context.dispatch(types.STORE, user);
            }
        },
        [types.REMOVE](context, user) {
            return service.remove(user.id).then((response) => {
                context.commit(types.REMOVE, user);
                return response
            })
        },
        [types.STORE](context, data) {
            return service.create(data).then((response) => {
                context.commit(types.STORE, response);
                return response
            })
        },
        [types.UPDATE](context, data) {
            return service.update(data.id, data).then((response) => {
                context.commit(types.UPDATE, response);
                return response
            })
        }
    },
    types: types
}